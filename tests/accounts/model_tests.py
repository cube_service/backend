import pytest

from accounts.models import User

pytestmark = pytest.mark.django_db


class TestUserModel:

    def test_create_user(self):
        user = User.objects.create_user('my_email@mail.com', '123')

        assert user.is_active
        assert user.is_staff is False
        assert user.is_superuser is False
        assert user.email == 'my_email@mail.com'

    def test_create_superuser(self):
        user = User.objects.create_superuser('my_email@mail.com', '123')

        assert user.is_active
        assert user.is_staff
        assert user.is_superuser
        assert user.email == 'my_email@mail.com'

    @pytest.mark.parametrize("email,name,expected_value", [
        ('my_email@mail.com', 'name', 'name'),
        ('my_email@mail.com', None, 'my_email@mail.com'),
    ])
    def test_get_full_name(self, email, name, expected_value):
        user = User.objects.create_user(email, '123', name=name)

        assert user.get_full_name() == expected_value

    @pytest.mark.parametrize("email,name,expected_value", [
        ('my_email@mail.com', 'name', 'my_email@mail.com'),
        ('my_email@mail.com', None, 'my_email@mail.com'),
    ])
    def test_get_short_name(self, email, name, expected_value):
        user = User.objects.create_user(email, '123', name=name)

        assert user.get_short_name() == expected_value
