from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django_registration.forms import RegistrationFormUniqueEmail

from .models import User


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('name', 'email')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('name', 'email')


class UserRegistrationForm(RegistrationFormUniqueEmail):

    class Meta(RegistrationFormUniqueEmail.Meta):
        model = User
