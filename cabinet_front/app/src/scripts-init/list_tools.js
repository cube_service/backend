import 'bootstrap/js/dist/modal';

$(document).ready(() => {
    $('#actions-bar').hide();

    $('.action-item').click(function () {
        let data = $(this).data();
        $('#actionModal h4.modal-title').text(data.modalTitle);
        $('#actionModal .modal-body').text(data.modalMessage);
        $('#actionModal .modal-footer a').text(data.modalActionButton);
        $('#actionModal .modal-footer a').attr('href', data.modalRedirect);
        $('#actionModal').modal();
    });

    $("[name='head_checkbox']").change(function() {
        if(this.checked) {
            $('#actions-bar').show();
            $(".list_checkbox").prop('checked', true);
        }
        else{
            $('#actions-bar').hide();
            $(".list_checkbox").prop('checked', false);
        }
    });

    $(".list_checkbox").change(function () {
        let show_actions_bar = false;
        $(".list_checkbox").each(function (index, element) {
            if($(element).prop('checked')) {
                show_actions_bar = true;
                return false
            }
        });
        if (show_actions_bar){
            $('#actions-bar').show();
        }else {
            $('#actions-bar').hide();
        }
    });

    $('.action-button').click(function () {
        let action = $(this).data('actionName');
        $("input[name='action']").val(action);
        $("#actions_form").submit();
    })


});