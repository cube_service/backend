// Toastr

import toastr from 'toastr';

$(document).ready(() => {
    $(function () {
        if(backend_messages){
            backend_messages.forEach(function(item) {
              toastr[item.tag](item.text)
            });
        }
    });

});