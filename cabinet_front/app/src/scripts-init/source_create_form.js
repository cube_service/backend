$(document).ready(() => {
    $("[for='id_import_yml_file']").parent().hide();
    $("[name='import_type']").change(function() {
        if($(this).val() === 'yml_file'){
            $("[for='id_import_yml_file']").parent().show();
            $("[for='id_import_yml_url']").parent().hide();
        }else{
            $("[for='id_import_yml_file']").parent().hide();
            $("[for='id_import_yml_url']").parent().show();
        }
    });
});