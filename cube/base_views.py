import copy

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, ListView, CreateView, UpdateView, DetailView, View
from pure_pagination import PaginationMixin


class CabinetMixin:
    title = ''
    page_head = ''
    head_icon = ''
    actions = []
    cabinet_menu = [
        {'type': 'head_item', 'name': 'Сервис'},
        {'type': 'item', 'name': 'Товары', 'slug': 'product', 'icon': 'box1', 'sub_menu': [
            {'name': 'Список товаров', 'url': '', 'slug': 'list'},
            {'name': 'Архив товаров', 'url': '', 'slug': 'archive'}
        ]},
        {'type': 'item', 'name': 'Источники', 'slug': 'sources', 'icon': 'cloud-upload', 'sub_menu': [
            {'name': 'Список источников', 'url': reverse_lazy('source_list'), 'slug': 'list'},
            {'name': 'Новый источник', 'url': reverse_lazy('source_create'), 'slug': 'new'},
            {'name': 'Архив источников', 'url': '', 'slug': 'archive'}
        ]},
        {'type': 'item', 'name': 'Витрины', 'slug': 'shop', 'icon': 'photo-gallery', 'sub_menu': [
            {'name': 'Список витрин', 'url': '', 'slug': 'list'},
            {'name': 'Новая витрина', 'url': '', 'slug': 'new'},
            {'name': 'Архив витрин', 'url': '', 'slug': 'archive'}
        ]},
        {'type': 'head_item', 'name': 'Профиль'},
        {'type': 'item', 'name': 'Мой профиль', 'slug': 'profile', 'icon': 'id', 'url': reverse_lazy('profile')},
        {'type': 'item', 'name': 'Выход', 'slug': '', 'icon': 'power', 'url': reverse_lazy('logout')}

    ]
    active_menu_items = ''

    def get_head(self):
        return self.page_head

    def get_title(self):
        return self.title

    def get_icon(self):
        return self.head_icon

    def get_actions(self):
        return self.actions

    def get_active_menu_items(self):
        return self.active_menu_items

    def _get_cabinet_menu(self):
        active_menu = self.get_active_menu_items()
        cabinet_menu = copy.deepcopy(self.cabinet_menu)
        if active_menu:
            active_menu_items = active_menu.split(':')
            if len(active_menu_items) == 2:
                top_active_item, sub_item_active = active_menu_items[0], active_menu_items[1]
            elif len(active_menu_items) == 1:
                top_active_item, sub_item_active = active_menu_items[0], None
            else:
                top_active_item, sub_item_active = None, None
            if top_active_item:
                for menu_item in cabinet_menu:
                    if menu_item.get('slug') and menu_item['slug'] == top_active_item:
                        menu_item['active'] = True
                        if sub_item_active:
                            for sub_item in menu_item['sub_menu']:
                                if sub_item['slug'] == sub_item_active:
                                    sub_item['active'] = True
                                    break
                        break
        return cabinet_menu

    def get_context_data(self, **kwargs):
        context = {'page': {
            'title': self.get_title(),
            'page_head': self.get_head(),
            'head_icon': self.get_icon(),
            'actions': self.get_actions()
        }, 'cabinet_menu': self._get_cabinet_menu()}
        print('menu')

        context.update(**kwargs)
        return super().get_context_data(**context)


class CabinetTemplateView(CabinetMixin, LoginRequiredMixin, TemplateView):
    pass


class CabinetFormView(CabinetMixin, LoginRequiredMixin, FormView):
    pass


class CabinetListView(CabinetMixin, LoginRequiredMixin, PaginationMixin, ListView):
    pass


class CabinetCreateView(CabinetMixin, LoginRequiredMixin, CreateView):
    pass


class CabinetUpdateView(CabinetMixin, LoginRequiredMixin, UpdateView):
    pass


class CabinetDetailView(CabinetMixin, LoginRequiredMixin, DetailView):
    pass


class CabinetActionView(CabinetMixin, LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action', '')
        id_list = request.POST.getlist('id_list')
        action_function_name = f'view_action_{action}'
        if hasattr(self, action_function_name):
            return getattr(self, action_function_name)(request, id_list)
        else:
            return redirect('/')

