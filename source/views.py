from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import View

from cube.base_views import CabinetListView, CabinetCreateView, CabinetUpdateView, CabinetDetailView, CabinetActionView
from source.forms import SourceCreateForm, SourceChangeForm
from source.models import Source


class SourceListView(CabinetListView):
    template_name = 'source_list.html'
    title = 'Источники товаров'
    page_head = title
    head_icon = 'cloud-upload'
    active_menu_items = 'sources:list'
    context_object_name = 'sources'
    queryset = Source.objects.active()
    paginate_by = 20

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


class SourceCreateView(CabinetCreateView):
    template_name = 'source_create.html'
    title = 'Новый источник'
    page_head = title
    head_icon = 'plus'
    active_menu_items = 'sources:new'
    form_class = SourceCreateForm
    success_url = reverse_lazy('source_list')

    def form_valid(self, form):
        source = form.save(commit=False)
        source.user = self.request.user
        source.save()
        messages.add_message(self.request, messages.SUCCESS, 'Источник создан')
        return super().form_valid(form)


class SourceEditView(CabinetUpdateView):
    template_name = 'source_change.html'
    head_icon = 'pen'
    active_menu_items = 'sources:list'
    form_class = SourceChangeForm
    queryset = Source.objects.active()
    success_url = reverse_lazy('source_list')

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_title(self):
        source = self.get_object()
        return f'Редактирование источника: "{source.name}"'

    def get_head(self):
        return self.get_title()

    def form_valid(self, form):
        form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Источник изменен')
        return super().form_valid(form)


class SourceDetailView(CabinetDetailView):
    template_name = 'source_detail.html'
    head_icon = 'note2'
    active_menu_items = 'sources:list'
    queryset = Source.objects.active()

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_title(self):
        source = self.get_object()
        return f'Редактирование источника: "{source.name}"'

    def get_head(self):
        return self.get_title()


class SourceDeleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        source = get_object_or_404(Source, pk=kwargs['source_id'], user=request.user)
        source.to_archive()
        messages.success(request, 'Источник "{}" был перемещен в архив'.format(source.name))
        return HttpResponseRedirect(reverse('source_list'))


class SourceActionView(CabinetActionView):

    def view_action_delete(self, request, id_list):
        Source.objects.active().filter(user=request.user, pk__in=id_list).update(archived=True)
        messages.success(request, "Источники были перемещены в архив")
        return HttpResponseRedirect(reverse('source_list'))
