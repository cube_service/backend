from urllib.parse import urlparse


def get_url_path(url):
    parsed_url = urlparse(url)
    return parsed_url.path


def get_redirect_location(response):
    # Due to Django 1.8 compatibility, we have to handle both cases
    return get_url_path(response['Location'])


def assert_cabinet_context(head, title, icon, actions, menu_slug, response_context):
    assert response_context['page']['page_head'] == head
    assert response_context['page']['head_icon'] == icon
    assert response_context['page']['title'] == title
    assert response_context['page']['actions'] == actions

    menu_items = menu_slug.split(':')
    if len(menu_items) == 2:
        top_active_item, sub_item_active = menu_items[0], menu_items[1]
    elif len(menu_items) == 1:
        top_active_item, sub_item_active = menu_items[0], None
    else:
        top_active_item, sub_item_active = None, None

    if top_active_item:
        for menu_item in response_context['cabinet_menu']:
            if menu_item.get('slug') and menu_item['slug'] == top_active_item:
                assert menu_item.get('active', False)
                if sub_item_active:
                    for sub_item in menu_item['sub_menu']:
                        if sub_item['slug'] == sub_item_active:
                            assert sub_item.get('active', False)
                        else:
                            assert not sub_item.get('active', False)
            else:
                assert not menu_item.get('active', False)
