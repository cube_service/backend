
var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
const ExtractSASS = new ExtractTextPlugin('./[name].css');

module.exports = {
    context: __dirname,
    entry: {
            main: './app/src/app.js',
            demo: './app/src/scripts-init/demo.js',
            toastr: './app/src/scripts-init/toastr.js',
            scrollbar: './app/src/scripts-init/scrollbar.js',
            fullcalendar: './app/src/scripts-init/calendar.js',
            maps: './app/src/scripts-init/maps.js',
            chart_js: './app/src/scripts-init/charts/chartjs.js',
            source_create_form: './app/src/scripts-init/source_create_form.js',
            list_tools: './app/src/scripts-init/list_tools.js',
        },
    output: {
      path: path.resolve('./app/bundle/'),
      filename: "[name]-[hash].js",
    },

    plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Tether: 'tether',
                'window.Tether': 'tether',
                Popper: ['popper.js', 'default'],
            }),
            new BundleTracker({filename: './webpack-stats.json'}),
            ExtractSASS
        ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/i,
                use: ExtractSASS.extract(['css-loader', 'sass-loader'])
            }, {
                test: /\.css$/i,
                use: ExtractSASS.extract(['css-loader'])
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './fonts'
                    }
                }]
            },
            {
                test: /\.(gif|jpg|png)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: './images'}
            }
        ]
    }

};