from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse_lazy

from cube.base_views import CabinetTemplateView, CabinetFormView


class ProfileView(CabinetTemplateView):
    template_name = 'profile.html'
    title = 'Профиль'
    page_head = title
    head_icon = 'id'
    active_menu_items = 'profile'


class PasswordChangeView(CabinetFormView):
    form_class = PasswordChangeForm
    success_url = reverse_lazy('profile')
    template_name = 'password_change_form.html'
    title = 'Изменение пароля'
    page_head = title
    head_icon = 'lock'
    active_menu_items = 'profile'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        messages.add_message(self.request, messages.SUCCESS, 'Пароль успешно изменен')
        return super().form_valid(form)
