from django import forms

from source.models import Source


class SourceCreateForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['import_type'] == 'yml_site' and not cleaned_data['import_yml_url']:
            self.add_error('import_yml_url', 'Нужно указать ссылку на yml файл')
        if cleaned_data['import_type'] == 'yml_file' and not cleaned_data['import_yml_file']:
            self.add_error('import_yml_file', 'Нужно загрузить файл yml')
        return cleaned_data

    class Meta:
        model = Source
        fields = ['name', 'description', 'import_type', 'import_yml_file', 'import_yml_url']


class SourceChangeForm(forms.ModelForm):

    class Meta:
        model = Source
        fields = ['name', 'description']
