import pytest

from accounts.models import User


@pytest.fixture
def user():
    return User.objects.create_user('my_email@mail.com', 'pass', name='NyName')
