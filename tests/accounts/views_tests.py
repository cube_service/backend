import pytest
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, PasswordChangeForm
from django.contrib.auth.models import AnonymousUser
from django.core import mail
from django_registration.forms import RegistrationForm

from accounts.forms import UserRegistrationForm
from accounts.models import User
from tests.utils import assert_cabinet_context


class TestLoginView:
    pytestmark = pytest.mark.django_db
    path = '/accounts/login/'

    def test_get_login_form(self, client):
        response = client.get(self.path)

        assert response.status_code == 200
        assert response.template_name == ['registration/login.html']
        assert isinstance(response.context['form'], AuthenticationForm)

    def test_success_login(self, client, user):
        response = client.post(self.path, {'username': user.email, 'password': 'pass'}, follow=True)

        assert response.status_code == 200
        assert response.context['request'].path == '/accounts/profile/'
        assert response.context['user'] == user

    @pytest.mark.parametrize('username, password, errors', [
        ('', '', {'username': ['Обязательное поле.'], 'password': ['Обязательное поле.']}),
        ('my_email@mail.com', '', {'password': ['Обязательное поле.']}),
        ('', 'pass', {'username': ['Обязательное поле.']}),
        ('my_email@', 'pass', {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                           'Оба поля могут быть чувствительны к регистру.']}),

        ('my_email@mail', 'pass', {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                               'Оба поля могут быть чувствительны к регистру.']}),

        ('@mail.com', 'pass', {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                           'Оба поля могут быть чувствительны к регистру.']}),

        ('myemail', 'pass', {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                         'Оба поля могут быть чувствительны к регистру.']})
    ])
    def test_invalid_login_fields(self, client, username, password, errors):
        response = client.post(self.path, {'username': username, 'password': password})

        assert response.template_name == ['registration/login.html']
        assert isinstance(response.context['form'], AuthenticationForm)
        assert response.context['form'].errors == errors
        assert isinstance(response.context['user'], AnonymousUser)

    @pytest.mark.parametrize('email, password', [
        ('my_email@mail.com', 'pass1'),
        ('my_email1@mail.com', 'pass'),
        ('my_email1@mail.com', 'pass1')
    ])
    def test_invalid_credentials(self, client, email, password):
        response = client.post(self.path, {'username': email, 'password': password})

        assert response.template_name == ['registration/login.html']
        assert isinstance(response.context['form'], AuthenticationForm)
        assert response.context['form'].errors == {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                                               'Оба поля могут быть чувствительны к регистру.']}
        assert isinstance(response.context['user'], AnonymousUser)

    def test_user_not_exist(self, client):
        response = client.post(self.path, {'username': 'my_email@mail.com', 'password': 'pass'})

        assert response.template_name == ['registration/login.html']
        assert isinstance(response.context['form'], AuthenticationForm)
        assert response.context['form'].errors == {'__all__': ['Пожалуйста, введите правильные email и пароль. '
                                                               'Оба поля могут быть чувствительны к регистру.']}
        assert isinstance(response.context['user'], AnonymousUser)


class TestRegistrationView:
    pytestmark = pytest.mark.django_db
    path = '/accounts/register/'

    def test_get_registration_form(self, client):
        response = client.get(self.path)

        assert response.status_code == 200
        assert response.template_name == ['django_registration/registration_form.html']
        assert isinstance(response.context['form'], RegistrationForm)

    def test_success_registration(self, client):
        response = client.post(self.path, {'email': 'email@email.com', 'password1': 'pass', 'password2': 'pass'},
                               follow=True)
        user = User.objects.first()

        assert response.status_code == 200
        assert response.context['request'].path == '/'
        assert response.context['user'] == user

    @pytest.mark.parametrize('email, password, password1, errors', [
        ('', '', '', {
            'email': ['Обязательное поле.'],
            'password1': ['Обязательное поле.'],
            'password2': ['Обязательное поле.']
        }),
        ('my_email@mail.com', '', '', {
            'password1': ['Обязательное поле.'],
            'password2': ['Обязательное поле.']
        }),
        ('', 'pass', '', {
            'email': ['Обязательное поле.'],
            'password2': ['Обязательное поле.'],
        }),
        ('', '', 'pass', {
            'email': ['Обязательное поле.'],
            'password1': ['Обязательное поле.']
        }),
        ('mail@mail.com', 'pass', 'no_pass', {'password2': ['Два поля с паролями не совпадают.']}),
        ('my_email@', 'pass', 'pass', {
            'email': ['Введите правильный адрес электронной почты.']
        }),
        ('my_email@mail', 'pass', 'pass', {
            'email': ['Введите правильный адрес электронной почты.']
        }),
        ('@mail.com', 'pass', 'pass', {
            'email': ['Введите правильный адрес электронной почты.']
        }),
        ('myemail', 'pass', 'pass', {
            'email': ['Введите правильный адрес электронной почты.']
        })
    ])
    def test_invalid_registration_fields(self, client, email, password, password1, errors):
        response = client.post(self.path, {'email': email, 'password1': password, 'password2': password1})

        assert response.template_name == ['django_registration/registration_form.html']
        assert isinstance(response.context['form'], UserRegistrationForm)
        assert response.context['form'].errors == errors
        assert isinstance(response.context['user'], AnonymousUser)

    def test_user_exist(self, client, user):
        response = client.post(self.path, {'email': user.email, 'password1': 'pass', 'password2': 'pass'})

        assert response.template_name == ['django_registration/registration_form.html']
        assert isinstance(response.context['form'], UserRegistrationForm)
        print(response.context['form'].errors)
        assert response.context['form'].errors == {'email': ['Этот адрес электронной почты уже используется. '
                                                             'Пожалуйста, введите другой адрес.']}
        assert isinstance(response.context['user'], AnonymousUser)


class TestProfileView:
    pytestmark = pytest.mark.django_db
    path = '/accounts/profile/'
    page_data = {
        'head': 'Профиль',
        'title': 'Профиль',
        'icon': 'id',
        'actions': [],
        'menu_slug': 'profile'
    }

    def test_success(self, user, client):
        client.force_login(user=user)
        response = client.get(self.path)
        self.page_data['response_context'] = response.context

        assert response.status_code == 200
        assert response.template_name == ['profile.html']
        assert response.context['user'] == user
        assert_cabinet_context(**self.page_data)

    def test_without_auth(self, client):
        response = client.get(self.path, follow=True)

        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['request'].path == '/accounts/login/'


class TestResetPasswordView:

    def test_get_template(self, client):
        response = client.get('/accounts/password_reset/')

        assert response.status_code == 200
        assert response.template_name == ['registration/password_reset_form.html']
        assert isinstance(response.context['form'], PasswordResetForm)

    @pytest.mark.django_db
    def test_success_send_mail(self, user, client):
        response = client.post('/accounts/password_reset/', {'email': user.email}, follow=True)

        assert response.status_code == 200
        assert response.context['request'].path == '/accounts/password_reset/done/'
        assert len(mail.outbox) == 1
        assert mail.outbox[0].subject == 'Восстановление пароля в сервисе Cube.'
        assert mail.outbox[0].to == [user.email]

    @pytest.mark.parametrize('email, errors', [
        ('', {'email': ['Обязательное поле.']}),
        ('my_email@', {'email': ['Введите правильный адрес электронной почты.']}),
        ('my_email@mail', {'email': ['Введите правильный адрес электронной почты.']}),
        ('@mail.com', {'email': ['Введите правильный адрес электронной почты.']}),
        ('myemail', {'email': ['Введите правильный адрес электронной почты.']})
    ])
    def test_invalid_email(self, client, email, errors):
        response = client.post('/accounts/password_reset/', {'email': email})

        assert response.status_code == 200
        assert response.context['request'].path == '/accounts/password_reset/'
        assert isinstance(response.context['form'], PasswordResetForm)
        assert response.context['form'].errors == errors
        assert len(mail.outbox) == 0

    @pytest.mark.django_db
    def test_user_not_exist(self, client):
        response = client.post('/accounts/password_reset/', {'email': 'sssss@sssss.com'})

        assert response.status_code == 302
        assert response.url == '/accounts/password_reset/done/'
        assert len(mail.outbox) == 0

    def test_password_reset_done(self, client):
        response = client.get('/accounts/password_reset/done/')

        assert response.status_code == 200
        assert response.template_name == ['registration/password_reset_done.html']


class TestChangePassword:
    path = '/accounts/change_password/'
    page_data = {
        'head': 'Изменение пароля',
        'title': 'Изменение пароля',
        'icon': 'lock',
        'actions': [],
        'menu_slug': 'profile'
    }

    @pytest.mark.django_db
    def test_get_template(self, user, client):
        client.force_login(user=user)
        response = client.get(self.path)

        assert_cabinet_context(response_context=response.context, **self.page_data)
        assert response.status_code == 200
        assert response.template_name == ['password_change_form.html']
        assert isinstance(response.context['form'], PasswordChangeForm)

    def test_get_template_no_auth(self, client):
        response = client.get(self.path, follow=True)

        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['request'].path == '/accounts/login/'

    @pytest.mark.django_db
    def test_change_password_success(self, user, client):
        client.force_login(user=user)
        response = client.post(self.path, {
            'old_password': 'pass',
            'new_password1': 'password',
            'new_password2': 'password'
        }, follow=True)

        user.refresh_from_db()
        messages_list = list(response.context['messages'])

        assert response.status_code == 200
        assert response.context['request'].path == '/accounts/profile/'
        assert response.context['user'] == user
        assert len(messages_list) == 1
        assert messages_list[0].level == messages.SUCCESS
        assert messages_list[0].message == 'Пароль успешно изменен'
        assert user.check_password('password')

    @pytest.mark.django_db
    @pytest.mark.parametrize('old_password, new_password1, new_password2, errors', [
        ('', '', '', {'new_password1': ['Обязательное поле.'], 'new_password2': ['Обязательное поле.'],
                      'old_password': ['Обязательное поле.']}),
        ('password', 'password', 'password', {'old_password': ['Ваш старый пароль введен неправильно. '
                                                               'Пожалуйста, введите его снова.']}),
        ('pass', 'password1', 'password', {'new_password2': ['Два поля с паролями не совпадают.']}),
        ('pass', 'password', 'password1', {'new_password2': ['Два поля с паролями не совпадают.']}),
    ])
    def test_change_password_invalid_data(self, user, client, old_password, new_password1, new_password2, errors):
        client.force_login(user=user)
        response = client.post(self.path, {
            'old_password': old_password,
            'new_password1': new_password1,
            'new_password2': new_password2
        }, follow=True)
        user.refresh_from_db()
        messages_list = list(response.context['messages'])

        assert response.status_code == 200
        assert response.context['request'].path == '/accounts/change_password/'
        assert response.context['user'] == user
        assert len(messages_list) == 0
        assert user.check_password('pass')
        assert response.context['form'].errors == errors

    def test_change_password_without_password(self, client):
        response = client.post(self.path, {
            'old_password': 'pass',
            'new_password1': 'password',
            'new_password2': 'password'
        }, follow=True)

        assert response.status_code == 200
        assert isinstance(response.context['user'], AnonymousUser)
        assert response.context['request'].path == '/accounts/login/'
