'use strict';

const Path = require('path');
const Webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractSASS = new ExtractTextPlugin('./[name]-[hash].css');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const webpack = require('webpack');

module.exports = (options) => {
    const dest = Path.join(__dirname, 'assets/bundle');

    let webpackConfig = {
        devtool: options.devtool,
        entry: {
            main: './assets/src/app.js',
            demo: './assets/src/scripts-init/demo.js',
            toastr: './assets/src/scripts-init/toastr.js',
            scrollbar: './assets/src/scripts-init/scrollbar.js',
            fullcalendar: './assets/src/scripts-init/calendar.js',
            maps: './assets/src/scripts-init/maps.js',
            chart_js: './assets/src/scripts-init/charts/chartjs.js',
        },
        output: {
            path: dest,
            filename: './assets/scripts/[name]-[hash].js'
        },
        plugins: [
            new Webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Tether: 'tether',
                'window.Tether': 'tether',
                Popper: ['popper.js', 'default'],
            }),
            new Webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development')
                }
            })
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: './assets/fonts'
                        }
                    }]
                },
                {
                    test: /\.(gif|jpg|png)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './assets/images'
                    }
                }
            ]
        }
    };

    webpackConfig.plugins.push(
        new BundleTracker({filename: './webpack-stats.json'})
    );

    if (options.isProduction) {
        webpackConfig.entry = [
            './assets/src/app.js',
            './assets/src/scripts-init/demo.js',
            './assets/src/scripts-init/toastr.js',
            './assets/src/scripts-init/scrollbar.js',
            './assets/src/scripts-init/calendar.js',
            './assets/src/scripts-init/maps.js',
            './assets/src/scripts-init/charts/chartjs.js',

        ];

        webpackConfig.plugins.push(
            ExtractSASS,
            new CleanWebpackPlugin([dest], {
                verbose: true,
                dry: false
            })
        );

        webpackConfig.module.rules.push({
            test: /\.scss$/i,
            use: ExtractSASS.extract(['css-loader', 'sass-loader'])
        }, {
            test: /\.css$/i,
            use: ExtractSASS.extract(['css-loader'])
        });

    } else {
        webpackConfig.plugins.push(
            new Webpack.HotModuleReplacementPlugin()
        );
        webpackConfig.module.rules.push({
            test: /\.scss$/i,
            use: ['style-loader?sourceMap', 'css-loader?sourceMap', 'sass-loader?sourceMap']
        }, {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.js$/,
            use: 'eslint-loader',
            exclude: /node_modules/
        });

        webpackConfig.devServer = {
            port: options.port,
            contentBase: dest,
            historyApiFallback: true,
            compress: options.isProduction,
            inline: !options.isProduction,
            hot: !options.isProduction,
            stats: {
                chunks: true
            }
        };

        webpackConfig.plugins.push(
            new BrowserSyncPlugin({
                host: 'localhost',
                port: 3001,
                files: ["public/**/*.*"],
                browser: "google chrome",
                reloadDelay: 1000,
            }, {
                reload: false
            })
        );

    }
    return webpackConfig;

};