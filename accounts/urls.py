from django.urls import path, include
from django_registration.backends.one_step.views import RegistrationView

from accounts.forms import UserRegistrationForm
from accounts.views import ProfileView, PasswordChangeView

urlpatterns = [
    path('register/', RegistrationView.as_view(success_url='/', form_class=UserRegistrationForm),
         name='django_registration_register'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('change_password/', PasswordChangeView.as_view(), name='change_password')
]