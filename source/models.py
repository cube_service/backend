from django.db import models

from accounts.models import User


class SourceManager(models.Manager):

    def active(self):
        return self.filter(archived=False)

    def archive(self):
        return self.filter(archived=True)


class Source(models.Model):
    IMPORT_TYPES_CHOICES = (
        ('yml_site', 'YML с сайта'),
        ('yml_file', 'YML файл')
    )

    name = models.CharField('Название источника', max_length=200)
    description = models.TextField('Описание', null=True, blank=True)
    user = models.ForeignKey(User, related_name='sources', on_delete=models.CASCADE)
    import_type = models.CharField('Тип импорта', choices=IMPORT_TYPES_CHOICES, default='yml_site', max_length=50)

    import_yml_url = models.URLField('Ссылка на yml файл', max_length=400, null=True, blank=True)
    import_yml_file = models.FileField('YML файл', upload_to='yml_files', null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)

    objects = SourceManager()

    def __str__(self):
        return self.name

    def to_archive(self):
        self.archived = True
        self.save()

    def to_active(self):
        self.archived = False
        self.save()
