### Build and install packages
FROM python:3.6
RUN mkdir /app
WORKDIR /app
RUN apt-get update && apt-get install -y gettext libgettextpo-dev
RUN pip install pipenv
COPY Pipfile Pipfile.lock /app/
RUN pipenv install --system --deploy --dev
ADD . /app/