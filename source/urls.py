from django.urls import path

from source.views import SourceListView, SourceCreateView, SourceEditView, SourceDetailView, SourceDeleteView, \
    SourceActionView

urlpatterns = [
    path('', SourceListView.as_view(), name='source_list'),
    path('create/', SourceCreateView.as_view(), name='source_create'),
    path('action/', SourceActionView.as_view(), name='source_action'),
    path('edit/<int:source_id>/', SourceEditView.as_view(), name='source_edit'),
    path('detail/<int:source_id>/', SourceDetailView.as_view(), name='source_detail'),
    path('delete/<int:source_id>/', SourceDeleteView.as_view(), name='source_delete'),
]